var express = require('express');
require('datejs')
var app = express();

var inc = function() {
	return  Math.round(Math.random() * 10)
}

app.get('/dau', function (req, res) {
  var result = []
  var date = Date.today()
  var fb = 0, play = 0, appstore = 0
  for (var i = 0; i < 30; ++i) {
  	var entry = {
  		datetime: date.format('%Y-%m-%d'),
  		fb: fb,
  		play: play,
  		appstore: appstore
  	}
  	result.push(entry)
  	date = date.add(1).day()
  	fb +=  inc()
  	play += inc()
  	appstore += inc()
  }
  res.send(result)
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
